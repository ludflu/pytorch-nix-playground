{ pkgs ? import <nixpkgs> {} }: 
  pkgs.stdenv.mkDerivation {
    name = "cuda-env-shell";
    buildInputs = with pkgs; [
      autoconf
      binutils
      curl
      freeglut
      git
      gitRepo
      gnumake
      gnupg
      gperf
      libGLU_combined
      linuxPackages.nvidia_x11
      m4
      ncurses5
      procps
      stdenv.cc
      unzip
      utillinux
      xorg.libX11
      xorg.libXext
      xorg.libXi
      xorg.libXmu
      xorg.libXrandr
      xorg.libXv
      zlib
      python36
      (python36.buildEnv.override {
            ignoreCollisions = true;
            extraLibs =  with python36Packages; [
                pytorchWithCuda
                torchvision
                matplotlib
                sympy
                numpy
                scipy
                pandas
                pillow
                scikitimage
                opencv
                virtualenvwrapper
                murmurhash
                
                #spacy

            #    jupyter
            #    ipykernel
            #    terminado


                cymem
                preshed
                thinc
                ujson
                dill
                regex
                msgpack
                cytoolz
                wrapt
                toolz

                #spacy
                #nvidia-ml-py3
                #tensorflowWithCuda
                #bcolz
                #keras
            ];
        })
    ];
      #export LANG=C.UTF-8
    shellHook = ''
      unset SOURCE_DATE_EPOCH
      export CUDA_PATH=${pkgs.cudatoolkit}
      export EXTRA_LDFLAGS="-L/lib -L${pkgs.linuxPackages.nvidia_x11}/lib"
      export EXTRA_CCFLAGS="-I/usr/include"
    '';
  }
